# -*- coding: utf-8 -*-
from django.test import TestCase
from django.contrib.auth.models import User

class MicroblogTest(TestCase):

    def test_200_Main_Page(self):
        response = self.client.get('/')
        assert response.status_code == 200

    def test_302_Add_Page(self):
        response = self.client.get('/add')
        assert response.status_code == 302

    def test_login_and_logout(self):
        self.CreateUser()
        response = self.client.get('/')
        self.assertContains(response, u'Zaloguj się')
        self.client.login(username='test', password='test')
        response = self.client.get('/')
        self.assertContains(response, u'Wyloguj się')

    def test_200_Register_Page(self):
        response = self.client.get('/register')
        assert response.status_code == 200

    def test_200_Login_Page_Correct_Pass(self):
        self.CreateUser()
        self.client.login(username='test', password='test')
        response = self.client.get('/')
        self.assertContains(response, u'<p><b>Jesteś zalogowany jako:</b> test</p>')

    def test_200_Login_Page_Wrong_Pass(self):
        self.client.login(username='test', password='test')
        response = self.client.get('/')
        self.assertNotContains(response, u'<p><b>Jesteś zalogowany jako:</b> test</p>')

    def test_user_list(self):
        self.CreateUser()
        response = self.client.get('/filtr')
        self.assertContains(response, u'<p><a href="/user/1">test</a></p>')

    @staticmethod
    def CreateUser():
        User.objects.create_user(username='test', password='test', email='test@test.com')
