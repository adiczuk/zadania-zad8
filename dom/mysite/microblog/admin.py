from django.contrib import admin
from models import Wpis, Tag

class WpisAdmin(admin.ModelAdmin):
    list_display = ['author', 'title', 'text', 'date', 'edited']
    fields = ['title', 'text', 'tags','editor', 'edited']
    ordering = ['-date',]
    list_filter = ['date']
    search_fields = ['tags__text']

class TagAdmin(admin.ModelAdmin):
    fields = ['text']
    search_fields = ['text']

admin.site.register(Wpis, WpisAdmin)
admin.site.register(Tag, TagAdmin)