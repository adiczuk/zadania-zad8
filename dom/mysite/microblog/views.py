# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from .models import Wpis, User, Tag, UserKey
from django.db.models import Q
from .forms import WpisForm, LoginForm, MyRegistrationForm
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.contrib.auth.decorators import login_required, permission_required
from django.core.mail import send_mail
from uuid import uuid4 as uuid

def index(request):
    info = request.session.get('info', None)
    if info:
        del request.session['info']
    wpisy = Wpis.objects.all().order_by('-pk')
    tablica = Tablica(request, wpisy)
    return render(request, 'microblog/main.html', {"wpisy" : wpisy, "info" : info, "tablica" : tablica})

def dodaj_wpis(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            form = WpisForm(initial={'author': request.user, 'date': timezone.now(), 'edited': None})
            return render(request, 'microblog/add.html', {"user_nick" : request.user.username, "form" : form})
        request.session['info'] = u'Najpierw się zaloguj!'
        return HttpResponseRedirect(reverse('login'))

    elif request.method == 'POST':
        form = WpisForm(request.POST)

        if form.is_valid():
            wpis = form.save()
            request.session['info'] = u'Dodano wpis!'
            return HttpResponseRedirect(reverse('index'))
        return render(request, 'microblog/add.html', {"form" : form})

def zaloguj(request):
    info = request.session.get('info', None)

    if request.method == 'GET':
        if request.user.is_authenticated():
            request.session['info'] = u'Jesteś już zalogowany!'
            return HttpResponseRedirect(reverse('index'))
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'microblog/login.html', {"info" : info, 'form' : form})

    elif request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        try:
            is_activte = UserKey.objects.get(user=user).is_active
        except:
            is_activte = True

        if user and is_activte:
            login(request, user)
            request.session['info'] = u'Zostałeś pomyślnie zalogowany!'
            return HttpResponseRedirect(reverse('index'))
        elif is_activte:
            info = u'Złe dane!'
        else:
            info = u'Najpierw potwierdź rejestrację!'
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'microblog/login.html', {"info" : info, 'form' : form})


def wyloguj(request):
    logout(request)
    request.session['info'] = u'Zostałeś wylogowany!'
    return HttpResponseRedirect(reverse('index'))

def rejestracja(request):
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            #wysyłanie emaila
            WyslijMail(request.POST['username'])
            request.session['info'] = u'Twoje konto zostało zarejestrowane! Wejdź na swoją skrzynkę mailową, aby potwierdzić rejestrację!'
            return HttpResponseRedirect(reverse('index'))
    else:
        form = MyRegistrationForm()
    return render(request, "microblog/register.html", {'form': form})

def moje_wpisy(request):
    if request.user.is_authenticated():
        author = User.objects.get(username=request.user.username)
        wpisy = Wpis.objects.all().order_by('-pk').filter(author=author)
        tablica = Tablica(request, wpisy)
        return render(request, 'microblog/mine.html', {"wpisy" : wpisy, "tablica": tablica})
    else:
        request.session['info'] = u'Najpierw się zaloguj!'
        return HttpResponseRedirect(reverse('login'))

def filtruj(request):
    users = User.objects.all().order_by('username')
    return render(request, 'microblog/users.html', {"users" : users})

def wpisy(request, pk):
    try:
        user = User.objects.get(pk=pk)
    except:
        return render(request, 'microblog/users_posts.html', {"wpisy" : None, 'user_post' : None})
    wpisy = Wpis.objects.all().filter(author_id=pk).order_by('-pk')

    return render(request, 'microblog/users_posts.html', {"wpisy" : wpisy, 'user_post' : user})

def potwierdzenie(request, key):
    uk = UserKey.objects.get(key=key)
    uk.is_active = True
    uk.save()
    request.session['info'] = u'Potwierdzenie przebiegło pomyślnie! Teraz możesz się zalogować'
    return HttpResponseRedirect(reverse('index'))

def WyslijMail(username):
    user = User.objects.get(username=username)
    key = uuid().hex
    uk = UserKey(key=key, user=user, is_active=False)
    uk.save()

    od = 'microblog@django.pl'
    do = user.email#'adiczuk@gmail.com'

    subject = 'Potwierdzenie rejestracji'
    message = 'Dziekujemy za rejestacje na naszym microblogu '+ username +'!\n' \
    'Aby dokonczyc rejestracje prosimy o wejscie w ponizszy link:\n' \
    'Link : ' + 'http://127.0.0.1:8000/email/' + key

    send_mail(subject, message, od, [do])

@login_required(login_url='/login')
def edycja(request, pk):
    try:
        wpis = Wpis.objects.get(pk=pk)
        if ((timezone.now() - wpis.date).seconds / 60 < 10 and wpis.author == request.user) or CzyModerator(request):
            form = WpisForm(initial={'author': request.user, 'date': wpis.date, 'title': wpis.title, 'text':wpis.text, 'edited': None})
            return render(request, 'microblog/edit.html', {'form': form, 'id':wpis.pk})
        request.session['info'] = u'Nie możesz edytować tego wpisu!'
        return HttpResponseRedirect(reverse('index'))
    except:
        request.session['info'] = u'Taki wpis nie istnieje!'
        return HttpResponseRedirect(reverse('index'))

@login_required(login_url='/login')
def edytowanie(request, pk):
    if request.method == 'POST':
        form = WpisForm(request.POST)
        if form.is_valid():
            edytowany = form.save(commit=False)
            wpis = Wpis.objects.get(pk=pk)

            straznik = False
            if len(wpis.tags.all()) != len(form.cleaned_data['tags']):
                straznik = True
            else:
                for i in range(0, len(wpis.tags.all())):
                    if wpis.tags.all()[i].text != form.cleaned_data['tags'][i].text:
                        straznik = True
                        break

            if edytowany.title != wpis.title or edytowany.text != wpis.text or straznik:
                wpis.edited = timezone.now()
                wpis.editor = User.objects.get(pk=request.user.id)
                wpis.title = edytowany.title
                wpis.text = edytowany.text
                wpis.tags.clear()
                try:
                    for text in form.cleaned_data['tags']:
                        tag = Tag.objects.get(text=text)
                        wpis.tags.add(tag)
                except:
                    pass
                wpis.save()

                request.session['info'] = u'Edytowanio wpis!'
                return HttpResponseRedirect(reverse('index'))
        else:
            return render(request, 'microblog/edit.html', {"form" : form, "id": pk})
    return HttpResponseRedirect(reverse('index'))

def szukaj(request):
    tagi = Tag.objects.all()
    return render(request, 'microblog/tags.html', {"tags" : tagi})

def szukaj_tag(request, pk):
    wpisy = Wpis.objects.all().order_by('-pk').filter(tags__in=pk)
    tablica = Tablica(request, wpisy)
    return render(request, 'microblog/bytag.html', {"wpisy" : wpisy, "tablica": tablica})

def CzyModerator(request):
    perms = request.user.groups.all()
    for perm in perms:
        if perm.name == 'Moderator':
            return True
    return request.user.has_perm('microblog.change_wpis')

def Tablica(request, wpisy):
    tablica = []
    for wpis in wpisy:
        if (timezone.now() - wpis.date).seconds / 60 < 10:
            if wpis.author == request.user:
                tablica.append(wpis.pk)
        else:
            break
    return tablica
