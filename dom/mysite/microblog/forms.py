# -*- coding: utf-8 -*-
from django.forms import ModelForm, HiddenInput, Textarea, PasswordInput
from django import forms
from .models import Wpis
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class WpisForm(ModelForm):
    class Meta:
        model = Wpis
        fields = [
            'author', 'title', 'text', 'date', 'tags'
        ]
        widgets = {
            'text': Textarea(),
            'author': HiddenInput(),
            'date': HiddenInput(),
        }

    def clean_title(self):
        title = self.cleaned_data['title']
        if len(title) < 3 or len(title) > 40:
            raise forms.ValidationError(u"Długośc tytułu musi być z przedziału <3, 40>")
        return title

    def clean_text(self):
        text = self.cleaned_data['text']
        if len(text) < 10 or len(text) > 500:
            raise forms.ValidationError(u"Długośc wpisu musi być z przedziału <10, 500>")
        return text

class LoginForm(ModelForm):
    class Meta:
        model = User
        fields = [
            'username', 'password'
        ]
        widgets = {
            'password' : PasswordInput(),
        }

class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField(required = True)

    class Meta:
        model = User
        fields = [
            'username', 'email', 'password1', 'password2'
        ]

        def save(self, commit = True):
            user = super(MyRegistrationForm, self).save(commit = False)
            user.email = self.cleaned_data['email']

            if commit:
                user.save()

            return user

    def clean_email(self):
        email = self.cleaned_data['email']
        users = User.objects.all()
        for user in users:
            if email == user.email:
                raise forms.ValidationError(u"Podany adres e-mail jest już używany!")
        return email

    def clean_username(self):
        username = self.cleaned_data['username']
        dl = len(username)
        if dl < 3:
            raise forms.ValidationError(u"Login musi mieć co najmniej 3 znaki.")
        return username

    def clean_password1(self):
        password1 = self.cleaned_data['password1']
        dl = len(password1)
        if dl < 4:
            raise forms.ValidationError(u"Hasło musi mieć co najmniej 4 znaki.")
        return password1