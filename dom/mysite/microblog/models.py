# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class Tag(models.Model):
    text = models.CharField(max_length=40)

    def __unicode__(self):
        return self.text

class Wpis(models.Model):
    author = models.ForeignKey(User, related_name='author', unique=False)
    title = models.CharField(max_length=40)
    text = models.TextField(u'Tekst wpisu')
    date = models.DateTimeField(u'Data wpisu')
    tags = models.ManyToManyField(Tag, blank=True, null=True)
    edited = models.DateTimeField(u'Data edycji', null=True)
    editor = models.ForeignKey(User, null=True, related_name='editor', unique=False)

class UserKey(models.Model):
    key = models.CharField(max_length=50)
    user = models.OneToOneField(User)
    is_active = models.BooleanField()