# -*- coding: utf-8 -*-
from django.test import TestCase

class MicroblogTest(TestCase):

    def test_200_Main_Page(self):
        response = self.client.get('/')
        assert response.status_code == 200

    def test_302_Add_Page(self):
        response = self.client.get('/add/')
        assert response.status_code == 302

    def test_200_Register_Page(self):
        response = self.client.get('/register/')
        assert response.status_code == 200

    def test_200_Register_Page2(self):
        response = self.client.post('/register/', {'nick': 'michal', 'pass1': 'michal', 'pass2': 'michal'})
        assert response.status_code == 302

    def test_200_Login_Page_Correct_Pass(self):
        response = self.client.post('/login/', {'nick':'michal', 'password':'michal'})
        assert response.status_code == 302

    def test_200_Login_Page_Wrong_Pass(self):
        response = self.client.post('/login/', {'nick':'czacza', 'password':'czacza'})
        assert response.status_code == 302

    def test_200_Wrong_Page(self):
        response = self.client.post('/dnscjkncknsdcjksd/')
        assert response.status_code == 200