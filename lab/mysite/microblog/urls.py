from django.conf.urls import patterns, url, include
from .views import *

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', index, name='index'),
    url(r'^add$', dodaj_wpis, name='add'),
    url(r'^login$', zaloguj, name='login'),
    url(r'^logout$', wyloguj, name='logout'),
    url(r'^register$', rejestracja, name='register'),
    url(r'^mine$', moje_wpisy, name='mine'),
    url(r'^filt$', filtruj, name='filt'),
    url(r'^user/(?P<pk>\d+)$', wpisy, name='user'),

    url(r'^/$', index, name='index/'),
    url(r'^add/$', dodaj_wpis, name='add/'),
    url(r'^login/$', zaloguj, name='login/'),
    url(r'^logout/$', wyloguj, name='logout/'),
    url(r'^register/$', rejestracja, name='register/'),
    url(r'^mine/$',moje_wpisy, name='mine/'),
    url(r'^filt/$', filtruj, name='filt/'),
    url(r'^user/(?P<pk>\d+)/$', wpisy, name='user/'),

    url(r'^', wrong, name='wrong'),
)