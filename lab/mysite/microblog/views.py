# -*- coding: utf-8 -*-
from .models import Wpis, User, Tag
from .forms import WpisForm
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils import timezone

def wrong(request):
    return render_to_response('microblog/wrong.html')

def index(request):
    info = request.session.get('info', None)
    if info:
        del request.session['info']
    user_nick = request.session.get('user_nick', None)
    return render_to_response('microblog/main.html', {"wpisy" : Wpis.objects.all().order_by('-pk'), "info" : info, "user_nick" : user_nick})

def dodaj_wpis(request):
    if request.method == 'GET':
        if request.session.get('user_nick', None):
            info = request.session.get('info', None)
            if info:
                del request.session['info']
            form = WpisForm()
            return render_to_response('microblog/add.html', {"user_nick" : request.session['user_nick'], "form" : form, "error" : info})
        request.session['info'] = u'Najpierw się zaloguj!'
        return HttpResponseRedirect(reverse('blog:login'))

    elif request.method == 'POST':
        form = WpisForm(request.POST)

        if form.is_valid():
            title = form.cleaned_data['title']
            text = form.cleaned_data['text']
            tags = form.cleaned_data['tags'].split(';')
            user = User.objects.get(nick=request.session['user_nick'])

            w = Wpis(title=title, text=text)
            w.author = user
            w.date = timezone.now()

            #print(w.date)
            w.save()
            for tag in tags:
                t = Tag(text=tag, wpis=w)
                t.save()
            request.session['info'] = u'Dodano wpis!'
            return HttpResponseRedirect(reverse('blog:index'))
        request.session['info'] = u"Długośc wpisu musi być z przedziału <10, 200>"
        return HttpResponseRedirect(reverse('blog:add'))

def zaloguj(request):
    info = request.session.get('info', None)

    if request.method == 'GET':
        if request.session.get('user_nick', None):
            request.session['info'] = u'Jesteś już zalogowany!'
            return HttpResponseRedirect(reverse('blog:index'))
        return render_to_response('microblog/login.html', {"info" : info})

    elif request.method == 'POST':
        nick = request.POST['nick']
        password = request.POST['password']

        userzy = User.objects.all()
        for user in userzy:
            if user.nick == nick and user.password == password:
                request.session['user_nick'] = user.nick
                request.session['info'] = u'Zostałeś zalogowany!'
                return HttpResponseRedirect(reverse('blog:index'))
        request.session['info'] = u'Błędne dane!'
        return HttpResponseRedirect(reverse('blog:login'))

def wyloguj(request):
    #wylogowanie
    if request.session['user_nick']:
        del request.session['user_nick']
    request.session['info'] = u'Zostałeś wylogowany!'
    return HttpResponseRedirect(reverse('blog:index'))

def rejestracja(request):
    info = request.session.get('info', None)

    if request.method == 'GET':
        if request.session.get('user_nick', None):
            request.session['info'] = u'Jesteś już zarejestrowany!'
            return HttpResponseRedirect(reverse('blog:index'))
        return render_to_response('microblog/register.html', {"info" : info})
    elif request.method == 'POST':
        #sprawdzanie poprawnosci
        nick = request.POST['nick']
        pass1 = request.POST['pass1']
        pass2 = request.POST['pass2']

        if pass1 and pass2 and nick:
            userzy = User.objects.all()
            for user in userzy:
                if user.nick == nick:
                    request.session['info'] = u'Podany login jest już zajęty!'
                    return HttpResponseRedirect(reverse('blog:register'))

            if pass1 == pass2:
                u = User(nick=nick, password=pass1)
                u.save()
                request.session['info'] = u'Rejestracja zakończona pomyślnie. Możesz się zalogować!'
                return HttpResponseRedirect(reverse('blog:index'))
            else:
                request.session['info'] = u'Hasła są różne!'
                return HttpResponseRedirect(reverse('blog:register'))
        else:
            request.session['info'] = u'Wypełnij wszystkie pola!'
            return HttpResponseRedirect(reverse('blog:register'))

def moje_wpisy(request):
    if request.session.get('user_nick', None):
        author = User.objects.get(nick=request.session['user_nick'])
        wpisy = Wpis.objects.all().order_by('-pk').filter(author=author)
        return render_to_response('microblog/mine.html', {"wpisy" : wpisy, "user_nick" : request.session['user_nick']})
    else:
        request.session['info'] = u'Najpierw się zaloguj!'
        return HttpResponseRedirect(reverse('blog:login'))

def filtruj(request):
    users = User.objects.all().order_by('nick')
    return render_to_response('microblog/users.html', {"users" : users})

def wpisy(request, pk):
    try:
        user = User.objects.get(pk=pk)
    except:
        return render_to_response('microblog/users_posts.html', {"wpisy" : None, 'user' : None})
    wpisy = Wpis.objects.all().filter(author_id=pk).order_by('-pk')
    return render_to_response('microblog/users_posts.html', {"wpisy" : wpisy, 'user' : user})
