# -*- coding: utf-8 -*-

from django import forms

class WpisForm(forms.Form):
    title = forms.CharField(label='Temat', max_length=50, min_length=1)
    text = forms.CharField(label=u'Treść', widget=forms.Textarea)
    tags = forms.CharField(label='Tagi (oddziel średnikami)', max_length=100, min_length=1)

    def clean_text(self):
        text = self.cleaned_data['text']
        num_words = len(text)
        print(num_words)
        if num_words < 10 or num_words > 200:
            raise forms.ValidationError(u"Długośc wpisu musi być z przedziału <10, 200>")
        return text