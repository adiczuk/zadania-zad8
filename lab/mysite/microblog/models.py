from django.db import models

# Create your models here.

class User(models.Model):
    nick = models.CharField(max_length=20)
    password = models.CharField(('password'), max_length=128)

    def __unicode__(self):
        return self.nick

class Wpis(models.Model):
    author = models.ForeignKey(User)
    title = models.CharField(max_length=40)
    text = models.TextField('Tekst wpisu')
    date = models.DateField(u'Data wpisu')

    def __unicode__(self):
        return self.title

class Tag(models.Model):
    text = models.CharField(max_length=40)
    wpis = models.ManyToManyField(Wpis)

    def __unicode__(self):
        return self.text